function factorial(n) {
  let f = 1;
  for(let i = 1; i <= n; i++) f*=i;
  return f;
}

const cache = {};

const checkCache = (n) => {
  if(cache[n]) {
    return [cache[n], 'from cache'];
  } else {
    const value = factorial(n);
    cache[n] = value;
    return [value, 'calculated'];
  }
  
}

self.addEventListener("connect", e => {
  e.source.addEventListener("message", ev => {
    e.source.postMessage(checkCache(ev.data));
  }, false);
  e.source.start();
}, false);