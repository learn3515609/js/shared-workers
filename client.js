const myWorker = new SharedWorker("worker.js");

factorial.addEventListener('input', (e) => {
  const value = e.target.value;
  myWorker.port.postMessage(value);
  console.log('Number posted to worker:', value);
})


myWorker.port.addEventListener("message", e => {
  console.log('result', e.data);
}, false);
myWorker.port.start();
